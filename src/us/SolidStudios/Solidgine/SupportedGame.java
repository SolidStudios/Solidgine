/* Copyright 2014 Walt Pach, all rights reserved */

package us.SolidStudios.Solidgine;

public abstract class SupportedGame {
    public abstract void start();
    public abstract void restart();
    public abstract void stop();
    public abstract void run();
    public abstract String getName();
    public abstract String getVersion();
    public abstract String getAuthor();
}
