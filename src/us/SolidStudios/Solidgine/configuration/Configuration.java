/***************************************************************************************************************/
/** Copyright 2015 BiggerOnTheInside (development), all rights reserved.                                       */
/** Released under the Binder License (https://github.com/BiggerOnTheInside/Licenses/blob/master/Binder.txt)   */
/***************************************************************************************************************/

package us.SolidStudios.Solidgine.configuration;

public interface Configuration extends ConfigurationSection{
	public DefaultConfigurationOptions getOptions();
	public Configuration getDefaults();
	public void setDefaults(Configuration defaults);
	public void addDefault(String path, Object object);
}
