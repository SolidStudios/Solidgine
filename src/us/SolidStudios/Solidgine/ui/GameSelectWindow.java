/***************************************************************************************************************/
/** Copyright 2015 BiggerOnTheInside (development), all rights reserved.                                       */
/** Released under the Binder License (https://github.com/BiggerOnTheInside/Licenses/blob/master/Binder.txt)   */
/***************************************************************************************************************/

package us.SolidStudios.Solidgine.ui;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import us.SolidStudios.Solidgine.Engine;

import us.SolidStudios.Solidgine.SupportedGame;
import us.SolidStudios.Solidgine.loader.GameLoader;

public class GameSelectWindow extends JFrame implements ActionListener{
    private HashMap<String, SupportedGame> games;
    private JComboBox selectionBox;
    private JButton launchButton;
    private Container windowPane;
    private int count = 0;
    
    public GameSelectWindow(int width, int height) throws Exception{
    	games = new HashMap<String, SupportedGame>();
    	
        selectionBox = new JComboBox();
        launchButton = new JButton("Launch");
        windowPane = this.getContentPane();
            
        File gameFolder = new File(System.getProperty("solidgine.home") + "games/");
		
        for(File f : gameFolder.listFiles()){
            String s = f.getName();
            String tailessName = s.split(".jar")[0];

            if(!s.toLowerCase().contains(".ds")){
                System.out.println(tailessName);
                games.put(tailessName, GameLoader.loadGame(tailessName));
            }
        }

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setPreferredSize(new Dimension(900, 400));
        this.setVisible(true);
        this.pack();
        this.setLocationRelativeTo(null);
            
        for(String s : games.keySet()){
            selectionBox.addItem(s);
            count++;
        }
            
        launchButton.addActionListener(this);
        windowPane.setLayout(new FlowLayout());
        windowPane.add(selectionBox);
        windowPane.add(launchButton);
        this.pack();
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        SupportedGame game = games.get(selectionBox.getSelectedItem());

        Engine.getLogger().log(Level.INFO, "Starting Game: {0}, {1} by {2}", new Object[]{game.getName(), game.getVersion(), game.getAuthor()});
        game.start();
        
        this.removeAll();
        this.setVisible(false);
        this.dispose();
    }
}