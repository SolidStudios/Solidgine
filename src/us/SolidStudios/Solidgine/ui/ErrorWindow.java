/***************************************************************************************************************/
/** Copyright 2015 BiggerOnTheInside (development), all rights reserved.                                       */
/** Released under the Binder License (https://github.com/BiggerOnTheInside/Licenses/blob/master/Binder.txt)   */
/***************************************************************************************************************/

package us.SolidStudios.Solidgine.ui;

import javax.swing.JFrame;

public class ErrorWindow extends JFrame{
	private String error;
	
	public ErrorWindow(String error){
		this.error = error;
	}
}
