/* Copyright 2014 Walt Pach, all rights reserved */

package us.SolidStudios.Solidgine;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import us.SolidStudios.Solidgine.loader.GameLoader;
import us.SolidStudios.Solidgine.ui.GameSelectWindow;

public class Engine {
    public static String ENGINE_FOLDER = System.getProperty("user.home") + "/Solidgine/";
    
    private static Logger logger = Logger.getLogger("Solidgine");
    private static GameLoader gameLoader;
    
    public static void main(String[] args){
        System.setProperty("solidgine.home", ENGINE_FOLDER);

        try {
            GameSelectWindow window = new GameSelectWindow(300, 700);
        } catch (Exception ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Logger getLogger(){
        return logger;
    }
}