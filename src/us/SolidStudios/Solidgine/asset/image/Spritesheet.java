/* Copyright 2014 Walt Pach, all rights reserved */

package us.SolidStudios.Solidgine.asset.image;

public class Spritesheet {
    public static Spritesheet blockTextures = new Spritesheet("spritesheets/terrain.png", 16);

    private Texture texture;
    private String path;
    private float size;

    /**
     * @param path Path to image.
     * @param size Size in pixels of all textures. (eg. 8, 16, 32, 64...).
     */
    public Spritesheet(String path, float size){
        this.path = path;
        this.size = 1f / size;

        load();
    }

    private void load(){
        texture = Texture.loadTexture(path);
    }

    public void bind(){
        texture.bind();
    }

    public void unbind(){
        texture.unbind();
    }

    public void delete(){
        texture.delete();
    }

    /**
     * @return Size of the textures.
     */
    public float uniformSize(){
        return size;
    }
}
