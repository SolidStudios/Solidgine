/* Copyright 2014 Walt Pach, all rights reserved */

package us.SolidStudios.Solidgine.asset.shader;

import static org.lwjgl.opengl.GL20.*;

public class ShaderProgram {
    private int program, vShader, fShader;

    /**
     * @param vShader The ID of the vertex shader.
     * @param fShader The ID of the fragement shader.
     */
    public ShaderProgram(int vShader, int fShader){
        this.vShader = vShader;
        this.fShader = fShader;

        createShaderProgram();
    }

    private void createShaderProgram(){
        program = glCreateProgram();

        glAttachShader(program, vShader);
        glAttachShader(program, fShader);
        glLinkProgram(program);
        glValidateProgram(program);
    }

    public void use(){
        glUseProgram(program);
    }

    public void release(){
        glUseProgram(0);
    }

    public void dispose(){
        glDeleteProgram(program);
    }

    /**
     * @return The ID of the shader program.
     */
    public int getProgram() {
        return program;
    }
}
