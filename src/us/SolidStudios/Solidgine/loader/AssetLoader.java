/* Copyright 2014 Walt Pach, all rights reserved */

package us.SolidStudios.Solidgine.loader;

import java.io.InputStream;
import java.net.URL;

import us.SolidStudios.Solidgine.asset.image.Texture;
import us.SolidStudios.Solidgine.configuration.FileConfiguration;
import us.SolidStudios.Solidgine.configuration.JSONConfiguration;

/**
 * @author Walt
 * @version 0.0.1
*/
public class AssetLoader {
	public static String GAME_LIST = "games.json";
	
    /**
     * @param loc
     * @return 
     */
    public static InputStream getResourceAsStream(String loc){
        return AssetLoader.class.getResourceAsStream("assets/" + loc);
    }

    /**
     * @param loc
     * @return 
     */
    public static URL getResource(String loc){
        return AssetLoader.class.getResource("assets/" + loc);
    }
    
    public static Texture getTexture(String loc){
        return Texture.loadTexture("assets/" + loc);
    }
    
    public static FileConfiguration getConfiguration(String name){
    	return new JSONConfiguration(System.getProperty("solidgine.home") + "configuration/" + name);
    }
}
