/* Copyright 2014 Walt Pach, all rights reserved */

package us.SolidStudios.Solidgine.loader;

import java.net.URL;
import java.net.URLClassLoader;

import us.SolidStudios.Solidgine.SupportedGame;

public class GameLoader {
    public static SupportedGame loadGame(String gameName) throws Exception{
    	gameName = gameName.toLowerCase();
    	
        String gamesFolder = System.getProperty("solidgine.home") + "games/";
        String className = AssetLoader.getConfiguration(AssetLoader.GAME_LIST).getConfigurationSection(gameName).getString("main");
        URLClassLoader classLoader = URLClassLoader.newInstance(new URL[]{
        	new URL("file://" + gamesFolder + gameName + ".jar")	
        });
        
        Class<?> classMain = classLoader.loadClass(className);
        
        return ((SupportedGame)classMain.newInstance());
    }
}
